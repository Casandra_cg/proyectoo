<html>
<head>

    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>LOGGIN</title>


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

    <!--hoja de estilo para fondo de pantalla-->
   <link href="../../../../proyecto/public/css/estiloslogin.css" rel="stylesheet" type="text/css">

   <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">

   <script src="assets/js/jquery-login.js"></script>

    <link rel="stylesheet" type="text/css" media="screen" href="assets/css/dist/style.min.css" />

</head>
<body>
<center>
<!-- login area -->
    <style>
        h1{color: #f3ecec;}
    </style>
    <div class="row">
        <div class="col-md-3">
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
        </div>

        <div class="col-md-6 text-primary">
            <h1>ALMACEN DE ARTICULOS DE DEPORTE JOCAS</h1>
        </div>
    </div>
<aside id="login" class="col-sm-4">

    <!--<div class="card">
        <div class="card-body">
        -->
            <a href="index.php?controller=Usuario&action=registrar" class="float-right btn btn-primary">Registro</a>
            <center><h4 class="card-title mb-4 mt-1 text-white">INICIO DE SESION</h4></center> <!-- margins -->

            <hr />
            <!-- start form -->
            <form name="login_form" id="login_form" action="index.php?controller=Usuario&action=verificarCredenciales" method="POST">
                <div class="form-group">
                    <input name="correo" class="form-control" placeholder="Email or login" type="email" id="correo" >
                </div>
                <div class="form-group">
                    <input class="form-control" placeholder="******" type="password" id="contrasenia" name="contrasenia">
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <input class="float-right btn btn-primary" type="submit" value="Enviar">
                        </div>
                    </div>
                    <div class="col-md-6 pt-2 pr-4 btn btn-sucess text-right">
                        <a class="small" href="#" class="">¿Olvidó su contraseña?</a>
                    </div>
                </div>
                <div class="col-md-7 text-danger">
                <?php
                if (isset($estatus)){
                    echo "<h1>$estatus</h1>";
                }
                ?>
                </div>
            </form>

            <!-- messages area -->


        </div>
    </div>
</aside> <!-- end login area-->
</center>

</body>
</html>
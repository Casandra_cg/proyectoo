<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link href="../../../../proyecto/public/css/estilosregistro.css" rel="stylesheet" type="text/css">

    <title>Registar Usuario</title>
</head>
<body>

<form method="POST" action="index.php?controller=Usuario&action=agregar">
    <div id='container'>
        <img src="registros.jpg' alt="Registo'>
    </div>

    <div class="mx-auto " style="width:440px">
        <h1 class="text-primary">Crear Cuenta </h1>
    </div>
    <div class="container">

    <div class="form-row">
        <div class="form-group col-md-4">
            <label for="nombre">Nombre:</label>
            <input type="text" class="form-control" id="nombre" name="nombre" >
        </div>
        <div class="form-group col-md-3">
        </div>
        <div class="form-group col-md-4">
            <label for="apellidoP">Apellido Paterno:</label>
            <input type="text" class="form-control" id="apellidoP" name="apellidoP" >
        </div>
    </div>

    <div class="form-row">
        <div class="form-group col-md-4">
            <label for="apellidoM">Apellido Materno:</label>
            <input type="text" class="form-control" id="apellidoM" name="apellidoM" >
        </div>
        <div class="form-group col-md-3">
        </div>
        <div class="form-group col-md-4">
            <label for="telefono">Telefono:</label>
            <input type="text" class="form-control" id="telefono" name="telefono" >
        </div>
    </div>

    <div class="form-row">
        <div class="form-group col-md-4">
            <label for="correo">Correo:</label>
            <input type="text" class="form-control" id="correo" name="correo" >
        </div>
        <div class="form-group col-md-3">
        </div>
        <div class="form-group col-md-4">
            <label for="contrasenia">Contraseña :</label>
            <input type="text" class="form-control" id="contrasenia" name="contrasenia" >
        </div>
    </div>
    <div class="form-row">
        <div class="form-group col-md-3">
        </div>
        <div class="form-group col-md-3">
            <input class="btn btn-outline-success" type="submit" value="Registrar">
        </div>
        <div class="form-group col-md-3">
            <a href="?controller=Usuario&action=login" class="btn btn-outline-primary">Regresar a iniciar sesion</a>
        </div>
    </div>
</form>
</div>
</body>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>

</html>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <title>Actualizar</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link href="../../../../proyecto/public/css/estiloseditarU.css" rel="stylesheet" type="text/css">
</head>

<body>
///manda llamar la barra de navegacion
<header>
    <?php
    require_once ('header.php');
    ?>
</header>


<div class='jumbotron text-center'>
    <h1 class='text-primary'>Editar</h1>
</div>
<div class="container">
    <form action="index.php?controller=Usuario&action=editar"  method="POST" >
        <input id='id' type='hidden' name='id' value='<?php if (isset($Usuario)) echo $Usuario->id ?>'>

        <div class="form-row">
            <div class="form-group col-md-4">
                <label class="bg-info" for="nombre">Nombre:</label>
                <input type="text" class="form-control" id="nombre" name="nombre" value="<?php echo $Usuario->nombre ?>">
            </div>
            <div class="form-group col-md-4">
                <label class="bg-info" for="apellidoP">Apellido Paterno:</label>
                <input type="text" class="form-control" id="precio" name="apellidoP" value="<?php echo $Usuario->apellidoPaterno?>">
            </div>
            <div class="form-group col-md-4">
                <label class="bg-info" for="apellidoM">Apellido Materno:</label>
                <input type="text" class="form-control" id="apellidoM" name="apellidoM" value="<?php echo $Usuario->apellidoMaterno?>">
            </div>
            <div class="form-group col-md-4">
                <label class="bg-info" for="telefono">Telefono:</label>
                <input type="text" class="form-control" id="telefono" name="telefono" value="<?php echo $Usuario->telefono?>">
            </div>
            <div class="form-group col-md-4">
                <label class="bg-info" for="correo">Correo:</label>
                <input type="text" class="form-control" id="correo" name="correo" value="<?php echo $Usuario->correo?>">
            </div>
            <div class="form-group col-md-4">
                <label class="bg-info" for="contrasenia">Contraseña:</label>
                <input type="text" class="form-control" id="contrasenia" name="contrasenia" value="<?php echo $Usuario->contrasenia?>">
            </div>
            <div class="form-row">
                <div class="col-sm-4 ">


                    <input class="btn btn-success" type="submit" value="Guardar">
                </div>
                <div class="col-sm-4 ">
                </div>
                <div class="col-sm-4 ">
                    <a href="?controller=Usuario&action=mostrar" class="btn btn-primary ">Regresar</a>
                </div>
            </div>
    </form>

</div>



</body>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>

</html>
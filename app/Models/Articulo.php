<?php


class Articulo extends Conexion
{
    public $id;
    public $nombre;//declaracion de varuables
    public $precio;

    public function __construct()
    {
        parent::__construct();
    }

//funcion para mostrar todos los registros de la base de datos
    static function mostrar(){
        $me = new Conexion();
        $pre = mysqli_prepare($me->conexion, "SELECT * FROM articulos");//pasar la sentencia sql para mostrar todos los registros
        $pre->execute();//se ejecuta el query
        $res = $pre->get_result();

        $articulos=[];//se crea un arreglo para pasarle todos los registros
        while ($Articulo=$res->fetch_object(Articulo::class)) {//devuelve los elementos

            array_push($articulos, $Articulo);// este array_push sirve para insertar un objeto en un arreglo es primero el arreglo y despues el objeto
        }
        return $articulos;//retorna el arreglo crado
    }
//funcion para buscar por id
    static function buscarId($id){
        $me = new Conexion();
        $pre = mysqli_prepare($me->conexion, "SELECT * FROM articulos where id=?");
        $pre->bind_param("i",$id);
        $pre->execute();//se ejecuta el query
        $res = $pre->get_result();
        return $res->fetch_object(Articulo::class);

    }
    //funcion para insertar un registro
    public function insert(){
        $pre = mysqli_prepare($this->conexion,"INSERT INTO articulos (nombre,precio) VALUES (?,?)");//pasar la sentencia sql para insertar un nuevo registro
        $pre ->bind_param("ss",$this->nombre,$this->precio);//le pasa los parametros de la sentencia
        $pre->execute();//se ejecuta el query


    }
    //funcion para actualizar un registro
     function update(){
        $me = new Conexion();
        $pre = mysqli_prepare($me->conexion, "UPDATE articulos SET nombre=?,precio=? WHERE id=?");//pasar la sentencia sql para actualizar un registro
        $pre-> bind_param("ssi" , $this->nombre, $this->precio,$this->id);//le pasa los parametros de la sentencia
        $pre->execute();//se ejecuta el query


    }
    //funcion para remover un registro
    static function remove($id){
        $me = new Conexion();
        $pre = mysqli_prepare($me->conexion, "DELETE FROM articulos WHERE id=?");//pasar la sentencia sql para eliminar un registro
        $pre-> bind_param("i",$id);//le pasa los parametros de la sentencia
        $pre-> execute();
        return true;
    }


}
<?php


class Usuario extends Conexion
{
    public $nombre;
    public $apellidoPaterno;
    public $apellidoMaterno;
    public $telefono;
    public $correo;
    public $contrasenia;
    public function __construct()
    {
        parent::__construct();
    }

    static function verificarUsuario($correo,$contrasenia){
        $me = new Conexion();
        $pre = mysqli_prepare($me->conexion,"SELECT * FROM usuario WHERE correo = ? AND contrasenia = ?");
        $pre->bind_param("ss",$correo,$contrasenia);
        $pre->execute();
        $resultado = $pre->get_result();
        return $resultado->fetch_object();//7se crea un objeto
    }
    static function mostrar(){
        $me = new Conexion();
        $pre = mysqli_prepare($me->conexion, "SELECT * FROM usuario");//pasar la sentencia sql para mostrar todos los registros
        $pre->execute();//se ejecuta el query
        $res = $pre->get_result();

        $usuarios=[];//se crea un arreglo para pasarle todos los registros
        while ($Usuario=$res->fetch_object(Usuario::class)) {//devuelve los elementos

            array_push($usuarios, $Usuario);// este array_push sirve para insertar un objeto en un arreglo es primero el arreglo y despues el objeto
        }
        return $usuarios;//retorna el arreglo crado
    }
    //funcion para buscar por id
    static function buscarId($id){
        $me = new Conexion();
        $pre = mysqli_prepare($me->conexion, "SELECT * FROM usuario where id=?");
        $pre->bind_param("i",$id);
        $pre->execute();//se ejecuta el query
        $res = $pre->get_result();
        return $res->fetch_object(Usuario::class);

    }
    //funcion para insertar un registro
    public function insert(){
        $pre = mysqli_prepare($this->conexion,"INSERT INTO usuario (nombre,apellidoPaterno,apellidoMaterno,telefono,correo,contrasenia) VALUES (?,?,?,?,?,?)");//pasar la sentencia sql para insertar un nuevo registro
        $pre ->bind_param("ssssss",$this->nombre,$this->apellidoPaterno,$this->apellidoMaterno,$this->telefono,$this->correo,$this->contrasenia);//le pasa los parametros de la sentencia
        $pre->execute();//se ejecuta el query

    }

    //funcion para actualizar un registro
    function update(){

        $pre = mysqli_prepare($this->conexion, "UPDATE usuario SET nombre=?,apellidoPaterno=?,apellidoMaterno=?,telefono=?,correo=?,contrasenia=? WHERE id=?");//pasar la sentencia sql para actualizar un registro
        $pre-> bind_param("ssssssi" , $this->nombre, $this->apellidoPaterno,$this->apellidoMaterno,$this->telefono,$this->correo,$this->contrasenia,$this->id);//le pasa los parametros de la sentencia
        $pre->execute();//se ejecuta el query


    }
}
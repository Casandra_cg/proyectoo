<?php
require "app/Models/Conexion.php";
require "app/Models/Usuario.php";


class UsuarioController
{
    public function __construct(){
        if ($_GET["action"]== "candado"){
            if (!isset($_SESSION["usuario"])){
                echo "No has iniciado sesion";
            }
        }
    }
    function login(){
        require "app/Views/loggin.php";
    }
//funcion para verificar credenciales
    public function verificarCredenciales(){
        if(isset($_POST["correo"]) && isset($_POST["contrasenia"])) {
            $correo = $_POST["correo"];
            $contrasenia = $_POST["contrasenia"];
            $verificar = Usuario::verificarusuario($correo, $contrasenia);
            if (!$verificar) {
                echo "Error, Usuario no Encontrado";
            } else {
                $_SESSION["usuario"] = "Hola!";
                $_SESSION["correo"] = $_POST["correo"];
                $_SESSION["contrasenia"] = $_POST["contrasenia"];
                require 'app/Views/tienda.php';
            }
        }
    }
    public function logout(){
        if(isset($_SESSION["usuario"])){
            unset($_SESSION["usuario"]);
            unset($_SESSION["correo"]);
            unset($_SESSION["contrasenia"]);
        }
        require 'app/Views/Loggin.php';
    }

    public function all()
    {
        $id = $_GET["id"];

        $Usuario = Usuario::buscarId($id);//manda a llamar una clase statica de Articulo
        require_once "app/Views/actualizarU.php";//requiere la vista

    }
    public function mostrar()
    {

        $Usuarios= Usuario::mostrar();//manda a llamar una clase statica de controllers

        require_once "app/Views/mostrarU.php";//requiere la vista

    }

    function registrar(){
        require "app/Views/registrar.php";
    }
    public function agregar(){
        if (isset($_POST)) {
            $usuario= new Usuario();//crea un objeto con una instancia
            $usuario->nombre=$_POST["nombre"];//asigna valores al objeto creado
            $usuario->apellidoPaterno=$_POST["apellidoP"];//asigna valores al objeto creado
            $usuario->apellidoMaterno=$_POST["apellidoM"];//asigna valores al objeto creado
            $usuario->telefono=$_POST["telefono"];//asigna valores al objeto creado
            $usuario->correo=$_POST["correo"];//asigna valores al objeto creado
            $usuario->contrasenia=$_POST["contrasenia"];//asigna valores al objeto creado
            $usuario->insert();//manda a llamar la funcion de articulo

            require "app/Views/registrar.php";//requiere la vista

        }
    }
    //funcion de editar usuario
    public function editar(){
        if (isset($_POST)) {
            $id = $_POST["id"];

            $usuario = Usuario::buscarId($id);//manda a llamar una clase statica de Articulo
            $usuario->nombre=$_POST["nombre"];//asigna valores al objeto creado
            $usuario->apellidoPaterno=$_POST["apellidoP"];//asigna valores al objeto creado
            $usuario->apellidoMaterno=$_POST["apellidoM"];//asigna valores al objeto creado
            $usuario->telefono=$_POST["telefono"];//asigna valores al objeto creado
            $usuario->correo=$_POST["correo"];//asigna valores al objeto creado
            $usuario->contrasenia=$_POST["contrasenia"];//asigna valores al objeto creado
            $usuario->update();

            header('Location: /proyecto/?controller=Usuario&action=all&id=' . $id);//lo redirecciona

        }

    }
}
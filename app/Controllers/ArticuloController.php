<?php

require "app/Models/Conexion.php";
require "app/Models/Articulo.php";

class ArticuloController
{

    public function index()
    {

        require_once "app/Views/tienda.php";//requiere la vista

    }
    public function all()
    {
            $id = $_GET["id"];

            $Articulo = Articulo::buscarId($id);//manda a llamar una clase statica de Articulo
            require_once "app/Views/actualizar.php";//requiere la vista

    }



    public function mostrar()
    {

        $Articulos= Articulo::mostrar();//manda a llamar una clase statica de controllers

        require_once "app/Views/mostrarA.php";//requiere la vista

    }
    public function agregar(){
        if (isset($_POST)) {
            $articulo= new Articulo();//crea un objeto con una instancia
            $articulo->nombre=$_POST["nombre"];//asigna valores al objeto creado
            $articulo->precio=$_POST["precio"];//asigna valores al objeto creado
            $articulo->insert();//manda a llamar la funcion de articulo

            header('Location: /proyecto/?controller=Articulo&action=index');//lo redirecciona

        }
    }

    public function editar(){
        if (isset($_POST)) {
            $id = $_POST["id"];

            $articulo = Articulo::buscarId($id);//manda a llamar una clase statica de Articulo
            $articulo->nombre = $_POST["nombre"];//asigna valores al objeto creado
            $articulo->precio = $_POST["precio"];//asigna valores al objeto creado
            $articulo->update();

            header('Location: /proyecto/?controller=Articulo&action=all&id=' . $id);//lo redirecciona

        }

    }

    public function del  (){
        $id = $_GET["id"];

        $Articulo = Articulo::buscarId($id);//manda a llamar una clase statica de Articulo
    }
    public function eliminar(){
        $id =$_POST["id"];//crea una variable para pasar el id a a funcion remover
        var_dump($id);
        $Articulo = Articulo::remove($id);//

        header('Location: /proyecto/index.php?controller=Articulo&action=del&id='.$id);//lo redirecciona
    }


}